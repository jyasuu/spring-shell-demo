package com.example.demo;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.primaryKey;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.DSL.val;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.jooq.Field;
import org.jooq.Param;
import org.jooq.SQLDialect;
import org.jooq.conf.ParamType;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

class DemoApplicationTests {

	@Test
	void contextLoads() {
		Field<Object> f1 = field("A");
		Field<Object> f2 = field("B");
		Field<Object> f3 = field("C");

		Param<String> p1 = val("A");
		Param<String> p2 = val("B");
		Param<String> p3 = val("C");

		Map map = new HashMap<Field,Object>();
		map.put(f1, p1);
		map.put(f2, p2);
		map.put(f3, p3);


		String sql = DSL.using(SQLDialect.POSTGRES).
		insertInto(table("A")).
		columns(f1,f2,f3).
		values("A","B","C").
		onConflict(f1).
		doUpdate().
		set(map).getSQL(ParamType.INLINED);

		assertEquals("insert into A (A, B, C) values ('A', 'B', 'C') on conflict (A) do update set A = 'A', B = 'B', C = 'C'", sql);
	}

}
