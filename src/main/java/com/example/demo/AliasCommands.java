package com.example.demo;


import org.springframework.context.annotation.Bean;
import org.springframework.shell.command.CommandRegistration;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class AliasCommands {

	private final static String DESCRIPTION = "main1 with main2 as alias";

	@ShellMethod(key = { "alias anno main1", "alias anno main2" }, group = "Alias Commands", value = DESCRIPTION)
	public String annoMain1() {
		return "Hello annoMain1";
	}

	@Bean
	public CommandRegistration regMain1() {
		return CommandRegistration.builder()
			.command("alias", "reg", "main1")
			.group("Alias Commands")
			.description(DESCRIPTION)
			.withAlias()
				.command("alias", "reg", "main2")
				.group("Alias Commands")
				.and()
			.withTarget()
				.function(ctx -> {
					return "Hello regMain1";
				})
				.and()
			.build();
	}

}