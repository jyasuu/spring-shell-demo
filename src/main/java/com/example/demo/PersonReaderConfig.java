package com.example.demo;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.example.demo.bean.Person;
import com.example.demo.item.PersonReader;

@Configuration
public class PersonReaderConfig {
    private static final Logger logger = LoggerFactory.getLogger(Person.class);

    @Value("${api.person}")
    private String personAPIUrl ;
    
    @Bean
    public ItemReader<Person> itemReader() {
        FlatFileItemReader<Person> reader = new FlatFileItemReader<>();
        //FileSystemResource
        reader.setResource(new ClassPathResource("people.csv"));
        reader.setLineMapper(new DefaultLineMapper<Person>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[]{"firstName", "lastName"});
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
                setTargetType(Person.class);
            }});
        }});
        return reader;
    }

    @Bean
    public PersonReader personReader() {
        return new com.example.demo.item.PersonReader(personAPIUrl);
    }

    @Bean
    public ItemWriter<Person> itemWriter() {
        return items -> {
            for (Person person : items) {
                // Perform writing logic here
                System.out.println("Writing: " + person.getFirstname() + " " + person.getLastname());
                logger.info("Writing: " + person.getFirstname() + " " + person.getLastname());
            }
        };
    }

    @Bean
    public ItemWriter<Person> jdbcWriter(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Person>()
            .dataSource(dataSource)
            .sql("INSERT INTO person (firstname, lastname) VALUES (:firstname, :lastname)")
            .beanMapped()
            .build();
    }


    @Bean
    public ItemProcessor<Person, Person> itemProcessor() {
        return person -> {
            Thread.sleep(1000);
            // Perform any processing here
            logger.info(person.toString());
            person.setFirstname(person.getFirstname() == null ? null : person.getFirstname().toUpperCase());
            person.setLastname(person.getLastname() == null ? null : person.getLastname().toUpperCase());
            return person;
        };
    }
}
