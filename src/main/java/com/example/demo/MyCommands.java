package com.example.demo;

import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import org.springframework.shell.standard.ShellOption;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.jooq.exception.DataAccessException;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;


import static org.jooq.impl.DSL.*;
import static org.jooq.SQLDialect.*;
import org.jooq.*;
import org.jooq.Record;
import org.jooq.conf.ParamType;
import org.jooq.impl.*;
import org.jooq.tools.StringUtils;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import org.snakeyaml.engine.v2.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.LockSupport;
import java.util.function.Consumer;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;


@ShellComponent
public class MyCommands {

    @ShellMethod("Add two integers together.")
    public int add(int a, int b) {
        return a + b;
    }

    @ShellMethod(value = "Add numbers.", key = "sum")
    public int add2(int a, int b) {
        return a + b;
    }

    private boolean connected;

    @ShellMethod("Connect to the server.")
    public void connect(String user, String password) {
        connected = true;
    }
 
    @ShellMethod("Download the nuclear codes.")
    @ShellMethodAvailability("availabilityCheck") 
    public void download() {
    }

    public Availability downloadAvailability() {
        return connected
            ? Availability.available()
            : Availability.unavailable("you are not connected");
    }

    @ShellMethod("Connect to the server.")
    public void threadTest() {
        Thread[] threads = new Thread[2];
        threads[0] = new Thread(() -> {
            for (int i = 0; i < 50; ++ i) {
            System.out.println("A");
            LockSupport.unpark(threads[1]);
            LockSupport.park();
            }
        });

        threads[1] = new Thread(() -> {
            for (int i = 0; i < 50; ++ i) {
                LockSupport.park();
                System.out.println("B");
                LockSupport.unpark(threads[0]);
            }
        });
        threads[0].start();
        
        threads[1].start();
    }

    

    /**
     * @param a
     * @param b
     * @return
     * @throws SQLException
     */
    @ShellMethod("Insert")
    public int insert(
    @ShellOption(value = {"-n", "--name"}) String name,
    @ShellOption(value = {"-j", "--jdbcUrl"}, defaultValue = "jdbc:postgresql://127.0.0.1:5432/postgres") String jdbcUrl,
    @ShellOption(value = {"-u", "--username"}, defaultValue = "postgres") String username,
    @ShellOption(value = {"-p", "--password"}, defaultValue = "postgres") String password ) throws SQLException {
 
         // Setup the database connection
         try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password)) {
            // Setup your database connection and DSLContext here
    
            DSLContext dslContext = DSL.using(connection,SQLDialect.POSTGRES); // Initialize your DSLContext
    
    
            try{
                // Insert data into the account table
                int insertedRows = dslContext
                .insertInto(table("account"), field(name("name"), SQLDataType.VARCHAR(255)), field(name("password"), SQLDataType.VARCHAR(255)),field(name("email"), SQLDataType.VARCHAR(255)))
                .values(name, "pass","abc@example.com")
                .execute();
                return insertedRows;
            } catch (DataAccessException e) {
                e.printStackTrace();
            }

         }

         return 0;
    }
    
    @SuppressWarnings("unchecked")
    @ShellMethod("Import")
    public int importYaml(String file) throws Exception {
        LoadSettings settings = LoadSettings.builder().build();
        Load load = new Load(settings);
        InputStream input = new FileInputStream(file);
        List<Map<String, Object>> rows = (List<Map<String, Object>>) load.loadFromInputStream(input);

        
         // JDBC URL, username, and password
         String jdbcUrl = "jdbc:postgresql://127.0.0.1:5432/postgres";
         String username = "postgres";
         String password = "postgres";
 
         // Setup the database connection
         try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password)) {
            // Setup your database connection and DSLContext here
    
            DSLContext dslContext = DSL.using(connection,SQLDialect.POSTGRES); // Initialize your DSLContext
    

            try{
                int insertedRows = 0;

                List<Field<Object>> fields;
                Object[] values;

                for(Map<String, Object> row: rows){
                    fields = iteratorToArray(row.keySet().stream().map(key -> field(name(key))).iterator());
                    values = row.keySet().stream().map(key -> row.get(key)).toArray();
                    // Insert data into the account table
                    insertedRows += dslContext
                    .insertInto(table("account"), fields)
                    .values(values)
                    .execute();
        
                }
                return insertedRows;
            } catch (DataAccessException e) {
                e.printStackTrace();
            }

         }

         return 0;


    }
        // Method to convert iterator to array
        private static List<Field<Object>> iteratorToArray(Iterator<Field<Object>> iterator) {
            List<Field<Object>> list = new ArrayList<>();
            iterator.forEachRemaining(list::add);
            return list;
        }

        private static List<Object> iteratorToArray2(Iterator<Object> iterator) {
            List<Object> list = new ArrayList<>();
            iterator.forEachRemaining(list::add);
            return list;
        }



    @ShellMethod("Query")
    public String query(String format) throws SQLException {
         // JDBC URL, username, and password
         String jdbcUrl = "jdbc:postgresql://127.0.0.1:5432/postgres";
         String username = "postgres";
         String password = "postgres";
 
         // Setup the database connection
         try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password)) {
            // Setup your database connection and DSLContext here
    
            DSLContext dslContext = DSL.using(connection,SQLDialect.POSTGRES); // Initialize your DSLContext
    
    
            try{
                Result<Record> result = dslContext.
                selectFrom(table("account")).
                orderBy(1,2,3,4,5).
                fetch();
                switch(format){
                    case "csv":
                    return result.formatCSV();
                    case "xml":
                    return result.formatXML();
                    case "chart":
                    return result.formatChart();
                    case "html":
                    return result.formatHTML();
                    case "insert":
                    return result.formatInsert(table("account"));
                    case "yaml":
                    {
                        List<Map<String, Object>> resultList = new ArrayList<>();
                        for (Record record : result) {
                            Map<String, Object> resultMap = new HashMap<>();
                            for (Field<?> field : record.fields()){
                                resultMap.put(field.getName(), record.getValue(field));
                            }
                            resultList.add(resultMap);
                            
                        }

                        // Convert the list of maps to YAML using SnakeYAML
                        DumperOptions options = new DumperOptions();
                        options.setDefaultFlowStyle(FlowStyle.BLOCK);
                        Yaml yaml = new Yaml(options);
                        return yaml.dump(resultList);
                    }
                    case "json":
                    default:
                    return result.formatJSON();
                }

            } catch (DataAccessException e) {
                e.printStackTrace();
            }

         }

         return "";
    }

    @SuppressWarnings("unchecked")
    @ShellMethod("database meta")
    public String databaseMeta(String format,
    @ShellOption(value = {"-c", "--config"}) String config,
    @ShellOption(value = {"-o", "--out"}, defaultValue = "./out") String out,
    @ShellOption(value = {"-j", "--jdbcUrl"}, defaultValue = "jdbc:postgresql://127.0.0.1:5432/postgres") String jdbcUrl,
    @ShellOption(value = {"-u", "--username"}, defaultValue = "postgres") String username,
    @ShellOption(value = {"-p", "--password"}, defaultValue = "postgres") String password ) throws Exception {

        InputStream input = new FileInputStream(config);
        Yaml yaml = new Yaml();
        Map<String, Object> yamlData = yaml.load(input);
 
         // Setup the database connection
         try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password)) {
            // Setup your database connection and DSLContext here
    
            DSLContext dslContext = DSL.using(connection,SQLDialect.POSTGRES); // Initialize your DSLContext
    
    
            try{

                for (Map.Entry<String, Object> entry : yamlData.entrySet()) {
                    String fileContent;
                    Map<String,Object> tableInfo = (Map<String,Object>)entry.getValue();
                    String order = (String)tableInfo.get("order");
                    if(order == null){
                        order = "";
                    }
                    order = order.trim();
                    String[] orderColumns = order.split("[,]",-1);
                    List<Map<String,String>> columnlist = (List<Map<String,String>>)tableInfo.get("columns");
                    List<Field<Object>> fields;
                    Map<String,Field<Object>> fieldMap = new HashMap<>();
                    columnlist.forEach(new Consumer<Map<String,String>>() {
                        @Override
                        public void accept(Map<String, String> t) {
                            Field<Object> f ;
                            if(t.get("is_track").equals("true"))
                            {
                                f = field(name(t.get("column_name")));

                            }
                            else{
                                Object var ; 
                                switch(t.get("type")){
                                    case "numeric":
                                        var = Long.parseLong(t.get("default"));
                                    case "string":
                                    default:
                                        var = t.get("default");
                                    break;

                                }
                                f = field(val((Object)var)).as(t.get("column_name"));
                            }
                            fieldMap.put(t.get("column_name"),f );
                        }
                    });


                    fields = iteratorToArray(columnlist.stream().map(column -> fieldMap.get(column.get("column_name"))).iterator());
                    List<Field<Object>> orderFields =  new ArrayList<>();

                    orderFields.addAll(iteratorToArray(Arrays.asList(orderColumns).stream().map(column -> fieldMap.get(column)).filter(f -> f != null).iterator()));

                    Result<Record> result;
                    if(orderFields.isEmpty()){
                        result = dslContext.select(fields).
                        from(table(entry.getKey())).
                        orderBy(fields).
                        fetch();
                    }
                    else{
                        result = dslContext.select(fields).
                        from(table(entry.getKey())).
                        orderBy(orderFields).
                        fetch();
                    }

                    String ext = "";
                    switch(format){
                        case "csv":
                        fileContent = result.formatCSV();
                        ext = ".csv";
                        break;
                        case "xml":
                        fileContent =  result.formatXML();
                        ext = ".xml";
                        break;
                        case "chart":
                        fileContent =  result.formatChart();
                        ext = ".chart";
                        break;
                        case "html":
                        fileContent =  result.formatHTML();
                        ext = ".html";
                        break;
                        case "insert":
                        Field<?>[] fieldArray = new Field[fields.size()];
                        fields.toArray(fieldArray);
                        fileContent =  result.formatInsert(table(entry.getKey()),fieldArray);
                        ext = ".sql";
                        break;
                        case "yaml":
                        {
                            List<Map<String, Object>> resultList = new ArrayList<>();
                            for (Record record : result) {
                                Map<String, Object> resultMap = new HashMap<>();
                                for (Field<?> field : record.fields()){
                                    resultMap.put(field.getName(), record.getValue(field));
                                }
                                resultList.add(resultMap);
                                
                            }

                            // Convert the list of maps to YAML using SnakeYAML
                            DumperOptions options = new DumperOptions();
                            options.setDefaultFlowStyle(FlowStyle.BLOCK);
                            yaml = new Yaml(options);
                            fileContent =  yaml.dump(resultList);
                        }
                        ext = ".yaml";
                        break;
                        case "json":
                        default:
                        fileContent =  result.formatJSON();
                        ext = ".json";
                        break;
                    }


                    {
                        
                        // Open the file for writing
                        FileWriter fileWriter = new FileWriter(out+"/"+ entry.getKey() + ext);
                        
                        // Wrap the FileWriter in a BufferedWriter for efficient writing
                        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                        
                        // Write content to the file
                        bufferedWriter.write(fileContent);
                        
                        // Close the BufferedWriter to flush and close the file
                        bufferedWriter.close();
                        
                    }

                }

            } catch (DataAccessException e) {
                e.printStackTrace();
            }

         }

         return "";
    }

    
    @SuppressWarnings("unchecked")
    @ShellMethod("diff data")
    public String diff(String format,
    @ShellOption(value = {"-c", "--config"}) String config,
    @ShellOption(value = {"--reference"}) String reference,
    @ShellOption(value = { "--target"}) String target,
    @ShellOption(value = {"-o", "--out"}, defaultValue = "./out") String out ) throws Exception {

        InputStream input = new FileInputStream(config);
        Yaml yaml = new Yaml();
        Map<String, Object> yamlData = yaml.load(input);

        for (Map.Entry<String, Object> entry : yamlData.entrySet()){
            
            Map<String,Object> tableInfo = (Map<String,Object>)entry.getValue();
            List<Map<String,String>> columnlist = (List<Map<String,String>>)tableInfo.get("columns");
            List<Field<Object>> fields;
            Map<String,Field<Object>> fieldMap = new HashMap<>();
            columnlist.forEach(new Consumer<Map<String,String>>() {
                @Override
                public void accept(Map<String, String> t) {
                    Field<Object> f ;
                    if(t.get("is_track").equals("true"))
                    {
                        f = field(name(t.get("column_name")));

                    }
                    else{
                        Object var ; 
                        switch(t.get("type")){
                            case "numeric":
                                var = Long.parseLong(t.get("default"));
                            case "string":
                            default:
                                var = t.get("default");
                            break;

                        }
                        f = field(val((Object)var)).as(t.get("column_name"));
                    }
                    fieldMap.put(t.get("column_name"),f );
                }
            });


            fields = iteratorToArray(columnlist.stream().map(column -> fieldMap.get(column.get("column_name"))).iterator());
            

            InputStream input2 = new FileInputStream(reference+"/"+ entry.getKey() + ".yaml");
            List<Map<String, Object>> yamlData2 = yaml.load(input2);
            Set<String> set1 = new HashSet<String>();
            for(Map<String, Object> yd2 : yamlData2){
                set1.add((String)yd2.get(tableInfo.get("key")));

            }


            InputStream input3 = new FileInputStream(target+"/"+ entry.getKey() + ".yaml");
            List<Map<String, Object>> yamlData3 = yaml.load(input3);
            Set<String> set2 = new HashSet<String>();
            for(Map<String, Object> yd3 : yamlData3){
                set2.add((String)yd3.get(tableInfo.get("key")));

            }

            set1.removeAll(set2);

            StringBuilder str = new StringBuilder();
            
            for(Map<String, Object> yd2 : yamlData2){
                if(set1.contains((String)yd2.get(tableInfo.get("key")))){
                    List<Object> values = iteratorToArray2(columnlist.stream().map(column -> yd2.get(column.get("column_name"))).iterator());
                    String sql = DSL.insertInto(table(entry.getKey()))
                    .columns(fields)
                    .values(values).getSQL(ParamType.INLINED);

                    str.append(sql);

                }

            }
            System.out.println(str);




        }

         return "";
    }


}

