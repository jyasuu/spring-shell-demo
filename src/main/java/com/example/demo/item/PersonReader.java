package com.example.demo.item;

import java.util.Arrays;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.example.demo.bean.Person;
import com.example.demo.bean.PersonResponse;

public class PersonReader implements ItemReader<Person> {
    
    private static final Logger logger = LoggerFactory.getLogger(PersonReader.class);
    

    private Iterator<Person> personIterator;


    private int loopCount;


    private String url;

    public PersonReader(String url) {
        logger.info("init");
        this.url = url;
        loopCount = 5;
    }

    @Override
    public Person read() throws Exception {
        if(personIterator == null)
        {
            logger.info("init null personIterator");
            fetchPersonIterator();
        }
        if (!personIterator.hasNext() && loopCount-- > 0) {
            logger.info("refetch personIterator");
            fetchPersonIterator();
        }
        
        if(personIterator.hasNext()){
            return personIterator.next();
        }
        return null;
    }

    private void fetchPersonIterator() {
        logger.info("fetch personIterator");
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<PersonResponse> response = restTemplate.getForEntity(this.url, PersonResponse.class);
        this.personIterator = response.getBody().getData().iterator();
    }
}