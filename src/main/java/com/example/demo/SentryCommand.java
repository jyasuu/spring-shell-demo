package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.stereotype.Component;

import io.sentry.Sentry;
import io.sentry.SentryLevel;

@Component
@ShellComponent
public class SentryCommand {

    @Value("${sentry.dsn}")
    String sentryDsn;

    @ShellMethod("Init Sentry")
    public void initSentry() throws Exception {
        
        Sentry.init(options -> {
            options.setEnableExternalConfiguration(true);
            // Configure the background worker which sends events to sentry:
            // Wait up to 5 seconds before shutdown while there are events to send.
            options.setShutdownTimeoutMillis(5000);

            // Enable SDK logging with Debug level
            options.setDebug(true);
            // To change the verbosity, use:
            options.setDiagnosticLevel(
                // By default it's DEBUG.
                // A good option to have SDK debug log in prod is to use only level
                // ERROR here.
                SentryLevel.DEBUG
            );
            options.setDsn(sentryDsn);
        });



    }


    @ShellMethod("Run the sample")
    public void runSentry(
        @ShellOption(value = {"-m", "--message"}, defaultValue = "Hello Sentry!") String message) throws Exception {
        // Note that all fields set on the context are optional. Context data is copied onto
        // all future events in the current context (until the context is cleared).

        // Record a breadcrumb in the current context. By default the last 100 breadcrumbs are kept.
        Sentry.addBreadcrumb("User made an action");

        // Add extra data to future events in this context.
        Sentry.setExtra("extra", "thing");

        // Add an additional tag to future events in this context.
        Sentry.setTag("tagName", "tagValue");

        /*
        This sends a simple event to Sentry using the statically stored instance
        that was created in the ``main`` method.
        */
        Sentry.captureMessage("This is a test");

        try {
            unsafeMethod(message);
        } catch (Exception e) {
            // This sends an exception event to Sentry using the statically stored instance
            // that was created in the ``main`` method.
            Sentry.captureException(e);
        }

    }


    /**
     * An example method that throws an exception.
     */
    void unsafeMethod(String externalMessage) {
        throw new UnsupportedOperationException("You shouldn't call this!" + externalMessage);
    }

    
}
