package com.example.demo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.stereotype.Component;

@ShellComponent
class ConversionCommands {

	@ShellMethod("Shows conversion using Spring converter")
	public Object conversionExample(DomainObject object) {
		return object;
	}

}

class DomainObject {
	private final String value;

	DomainObject(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "DomainObject [value=" + value + "]";
	}
}

@Component
class CustomDomainConverter implements Converter<String, DomainObject> {

	@Override
	public DomainObject convert(String source) {
		return new DomainObject(source);
	}
}