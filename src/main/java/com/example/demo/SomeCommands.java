package com.example.demo;


import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellCommandGroup;

@ShellCommandGroup("Other Commands")
public class SomeCommands {
	@ShellMethod(value = "This one is in 'Other Commands'")
	public void wizz() {}

	@ShellMethod(value = "And this one is 'Yet Another Group'",
		group = "Yet Another Group")
	public void last() {}
}