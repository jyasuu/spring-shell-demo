package com.example.demo;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.bean.Person;
import com.example.demo.item.PersonReader;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private ItemReader<Person> itemReader;

    @Autowired
    private PersonReader personReader;

    @Autowired
    private ItemProcessor<Person, Person> itemProcessor;

    @Autowired
    @Qualifier("jdbcWriter")
    private ItemWriter<Person> itemWriter;


    

    @Bean
    public Job sampleJob() {
        return jobBuilderFactory.get("sampleJob")
                .start(sampleStep())
                .build();
    }

    @Bean
    public Step sampleStep() {
        return stepBuilderFactory.get("sampleStep")
                .<Person, Person>chunk(10)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .build();
    }


    @Bean
    public Job sample2Job() {
        return jobBuilderFactory.get("sample2Job")
                .start(sample2Step())
                .build();
    }

    @Bean
    public Step sample2Step() {
        return stepBuilderFactory.get("sample2Step")
                .<Person, Person>chunk(5)
                .reader(personReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .build();
    }
}
