package com.example.demo.bean;

import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class PersonResponse {
    private String status;
    private Integer code;
    private Integer total;
    private List<Person> data;
}