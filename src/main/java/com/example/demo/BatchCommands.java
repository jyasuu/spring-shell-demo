package com.example.demo;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.stereotype.Component;

@Component
@ShellComponent
public class BatchCommands {

    //@Lazy
    @Autowired
    private JobLauncher jobLauncher;

    //@Lazy
    @Autowired
    private Job sampleJob;

    
    @Autowired
    private Job sample2Job;

    @ShellMethod("Run the sample batch job")
    public void runSampleJob() throws Exception {
        JobParameters jobParameters = new JobParametersBuilder()
                .addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(sampleJob, jobParameters);
    }

    @ShellMethod("Run the sample batch job")
    public void runSample2Job() throws Exception {
        JobParameters jobParameters = new JobParametersBuilder()
                .addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(sample2Job, jobParameters);
    }
}
