FROM azul/zulu-openjdk:8u382-8.72.0.17

MAINTAINER capsleo2000@gmail.com

# cd /app
WORKDIR /app

ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} app.jar

CMD ["java", "-jar", "-Xmx1024M", "/app/app.jar"]
